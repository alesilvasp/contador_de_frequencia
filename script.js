
const button = document.getElementById("countButton");
button.addEventListener("click", function () {
    // teu código vai aqui ... 

    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");

    const letterCounts = {};

    for (let i = 0; i < typedText.length; i++) {
        let currentLetter = typedText[i];
        // faça algo com cada letra 

        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }
    }


    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = `Letra/espaço: '${letter}' - Qtde: ${letterCounts[letter]} || `;
        span.innerText = textContent;
        const letters = document.getElementById("lettersDiv");
        letters.appendChild(span);

    }

    words = typedText.split(/\s/);
    const wordCount = {}

    for (let i = 0; i < words.length; i++) {
        let word = words[i];
        // faça algo com cada palavra 

        if (wordCount[word] === undefined) {
            wordCount[word] = 1;
        } else {
            wordCount[word]++;
        }
    }

    for (let letter in wordCount) {
        const span = document.createElement("span");
        const textContent = `Palavra '${letter}' - Qtde: ${wordCount[letter]} ||   `;
        span.innerText = textContent;
        const words = document.getElementById("wordsDiv");
        words.appendChild(span);
    }
});


/*
1 - Pegar cada letra do textarea;
2 - Comparar cada letra com o restante das palavras;
3 - Contar cada letra;
*/






//  ---------------------------------------------------------------------

let palavra = 'alexander pereira silva';
let array = {};

for (let i = 0; i < palavra.length; i++) {
    let letra = palavra[i];

    if (array[letra] === undefined) {
        array[letra] = 1;
    } else {
        array[letra]++;
    }
} words = palavra.split(/\s/);